const Koa = require('koa');
const app = new Koa();
const views = require('koa-views');
const path = require('path');

// Must be used before any router is used
app.use(views(__dirname + '/views', {
  map: {
    njk: 'nunjucks'
  }
}));

app.use(async (ctx) => {
  await ctx.render('home', {double: 'rainbow'});
});
