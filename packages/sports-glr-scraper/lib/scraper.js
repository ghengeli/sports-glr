'use strict';
const cheerio = require('cheerio');
/*
  - .sportGroup league attribute
  - .sportGroup -> .dateGroup -> .eventGroup ->
      .leagueByDate -> .eventLine .eventHeader ->
      .eventLines -> .content-(delayed, in-progress, final, scheduled, postponed) -> .event-holder -> .eventLine
  * for "Today" multiple .sportGroup, single .dateGroup
  * for "Sport" single .sportGroup, multiple .dateGroup
    - .eventLine .eventHeader -> ... -> .date
*/

/*
  scores
    - in progress score: .eventLine .status-in-progress -> .score-content -> .scorebox -> .score -> .score-periods (2) -> .current-score
    - current status (bot 7 / FINAL): .eventLine .status-in-progress -> .score-content -> .scorebox -> .status
    - final score: .eventLine .status-complete -> .score-content -> .scorebox -> .score -> .score-periods (2) -> .current-score
*/

var SPORTSBOOKS = {
  'BOOKMAKER' : 93
}
function gameInfo($, el) {
  var gameId = $(el).attr('id');
  var startTime = $(el).attr('rel');

  return {id : gameId, startTime : startTime};
}

function teamInfo($, el) {
// how is 'this' Array of scraper functions
  var awayTeam = $(el).find('.eventLine-team .eventLine-value:nth-child(1) .team-name').text();
  var awayTeamId = $(el).find('.eventLine-team .eventLine-value:nth-child(1) .team-name').attr('rel');
  var homeTeam = $(el).find('.eventLine-team .eventLine-value:nth-child(2) .team-name').text();
  var homeTeamId = $(el).find('.eventLine-team .eventLine-value:nth-child(2) .team-name').attr('rel');
  return {
    "away" : {
      "id" : awayTeamId,
      "name" : awayTeam
    },
    "home" : {
      "id" : homeTeamId,
      "name" : homeTeam
    }
  };
}

function teamInfoMLB($, el) {
  var teams = teamInfo($, el);
  /* MLB listings have city abbr - starting pitcher, only care about city abbr */
  var teamNameRegex = /^\w{2,3}/;
  var t1Regex = teamNameRegex.exec(teams.away.name);
  var t2Regex = teamNameRegex.exec(teams.home.name);
  if (t1Regex && t2Regex) {
    teams.away.abbr = t1Regex[0];
    teams.away.name = '<lookup team full name>'
    teams.home.abbr = t2Regex[0];
    teams.home.name = '<lookup team full name>'
  }
  else {
    console.warn('unable to parse MLB teams\n\t' + teams.away.name + '\n\t' + teams.home.name);
  }

  return teams;
}

function teamInfoNCAAF($, el) {
  var teams = teamInfo($, el);
  var ncaaRankingTeamNameRegex = /^(\(\d+\)\s)?(.*)/;
  var awayTeamRegexResult = ncaaRankingTeamNameRegex.exec(teams.away.name);
  var homeTeamRegexResult = ncaaRankingTeamNameRegex.exec(teams.home.name);
  if (awayTeamRegexResult && homeTeamRegexResult) {
    teams.away.name = awayTeamRegexResult[2];
    teams.home.name = homeTeamRegexResult[2];
  }

  return teams;
}

function teamInfoNCAAB($, el) {
  var teams = teamInfo($, el);
  var ncaaRankingTeamNameRegex = /^(\(\d+\)\s)?(.*)/;
  var awayTeamRegexResult = ncaaRankingTeamNameRegex.exec(teams.away.name);
  var homeTeamRegexResult = ncaaRankingTeamNameRegex.exec(teams.home.name);
  if (awayTeamRegexResult && homeTeamRegexResult) {
    teams.away.name = awayTeamRegexResult[2];
    teams.home.name = homeTeamRegexResult[2];
  }

  return teams;
}

function teamInfoNHL($, el) {
  var teams = teamInfo($, el);

  return teams;
}

function teamInfoNFL($, el) {
  var teams = teamInfo($, el);

  return teams;
}

function gameLine($, el) {
  var game = {};

  var line1 = $(el).find('.eventLine-book[rel="' + SPORTSBOOKS['BOOKMAKER'] + '"] div:nth-child(1)').text();
  var line2 = $(el).find('.eventLine-book[rel="' + SPORTSBOOKS['BOOKMAKER'] + '"] div:nth-child(2)').text();

  /* pick'em lines will be listed as 'Pk' and 'hooks' will be listed as ½
   * - clean these up into real numbers */
  var line1 = line1.replace(/½/, '.5').replace(/PK/, '0');
  var line2 = line2.replace(/½/, '.5').replace(/PK/, '0');
  let lineOddsRegex = /^(-?\d+\.?\d?)\s([\.\d]*)/
  let line1RegexResult = lineOddsRegex.exec(line1);
  let line2RegexResult = lineOddsRegex.exec(line2);
  /* assume processing the "merged" output of lines with both the spread and total
   *  - this will list the line for the favorite and the total in line with the other team */
  if (line1RegexResult && line2RegexResult) {
    if (line1RegexResult[1] > 0) {
      game.totalLine = line1RegexResult[1];
      game.homeLine = line2RegexResult[1];
      game.awayLine = line2RegexResult[1] * -1;
    }
    else {
      game.totalLine = line2RegexResult[1];
      game.homeLine = line1RegexResult[1] * -1;
      game.awayLine = line1RegexResult[1];
    }
  }

  return game;

}

function gameStatus($, el) {
  var game = {};

  var awayTeamScore = $(el).find('.score-content .scorebox .score .score-periods:nth-child(1) .current-score').text();
  var homeTeamScore = $(el).find('.score-content .scorebox .score .score-periods:nth-child(2) .current-score').text();
  game.score = {};
  game.score.asOf = new Date();
  game.score.away = awayTeamScore;
  game.score.home = homeTeamScore;

  var gameComplete = $(el).hasClass('status-complete');
  game.score.status = {};
  if (!gameComplete) {
    var gameStatusRegex = /(.*?)<br>(.*)/;
    var gameStatusHtml = $(this).find('.status-in-progress .score-content .scorebox .status').html();
    var gameStatusRegexResult = gameStatusRegex.exec(gameStatusHtml);
    if (gameStatusRegexResult && gameStatusRegexResult.length > 2) {
      game.score.status.part1 = gameStatusRegexResult[1];
      game.score.status.part2 = gameStatusRegexResult[2];
    }
    else {
      console.warn('unable to determine game status ' + gameStatusHtml);
    }
  }
  else {
    game.score.status.complete = true;
  }

  return game;
}

class Scraper {
  constructor(html, gamesSelector, scrapers) {
    this.html = html;
    this.scrapers = scrapers;
    this.gamesSelector = gamesSelector;
  }

  scrapeGame($) {
    var scrapers = this.scrapers;
    return function(i, el) {
      var game = {};
      for (let i=0; i<scrapers.length; i++) {
        var g = scrapers[i]($, el);
        Object.assign(game, g);
      }
      return game;
    }
  }

  *iterator() {
    let $ = cheerio.load(this.html);
    var games = $(this.gamesSelector);
    yield* games.map(this.scrapeGame($)).get();
  }
}



module.exports = exports = {
  createScraper : function (html, type) {
    console.log('creating scraper of type: ' + type);
    var gamesSelector = `.sportGroup[league="${type}"] .eventLine:not(.eventHeader)`;
    switch (type) {
      case 'NCAAF':
       return new Scraper(html, gamesSelector, [gameInfo, teamInfoNCAAF, gameLine]);
     case 'NFL':
       return new Scraper(html, gamesSelector, [gameInfo, teamInfoNFL, gameLine]);
     case 'NCAAB':
       return new Scraper(html, gamesSelector, [gameInfo, teamInfoNCAAB, gameLine, gameStatus]);
    }

  }
}
