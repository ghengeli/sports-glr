'use strict';

const axios = require('axios');
const fs = require('fs');
var path = require('path');

var getTestData = async function() {
  var dataFile = path.resolve('spec/fixtures', 'scrape-today-merged.html');

  return new Promise(function (resolve, reject) {
    fs.readFile(dataFile, 'utf8', function (error, result) {
      if (error) {
        reject(error);
      } else {
        resolve(result);
      }
    });
  });
}

var getLiveData = async function(url) {
  debugger;
    return new Promise(function(resolve, reject) {
      axios.get(url)
        .then(function(response) {
          resolve(response.data);
        })
        .catch(function(error) {
          reject(error);
        });
      });
}
exports.getLiveData = getLiveData;
exports.getTestData = getTestData;
