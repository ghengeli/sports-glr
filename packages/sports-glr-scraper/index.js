'use strict';

const dataReader = require('./lib/dataReader');
const scraper = require('./lib/scraper');
const chalk = require('chalk');

async function doScrape(url, sport) {
  try {
    console.log(chalk.red('requesting html from ' + url));
    // var html = await dataReader.getLiveData('https://www.sportsbookreview.com/betting-odds/merged/?date=20171009');
    var html = await dataReader.getLiveData(url);
    // var html = await dataReader.getTestData();
    console.log(chalk.green('receieved html'));
    let s = scraper.createScraper(html, sport);
    return s.iterator();
  }
  catch (err) {
    console.log('error! ' + err);
    console.trace(err);
    throw err;
  }
}

exports.scrape = doScrape;
