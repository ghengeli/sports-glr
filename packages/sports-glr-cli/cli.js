#!/usr/bin/env node
const scraper = require('sports-glr-scraper');
const chalk = require('chalk');

const todayURL = 'https://classic.sportsbookreview.com/betting-odds/merged/';

function getDateSpecificURL(date, sport) {
  var dateParam = '?date=' + date;
  var urlRoot = 'https://classic.sportsbookreview.com/betting-odds';

  switch (sport) {
    case 'NCAAF':
      urlRoot += '/college-football';
      break;
    case 'NFL':
      urlRoot += '/nfl-football';
      break;
    case 'NCAAB':
      urlRoot += '/ncaa-basketball'
      break;
  };
  return urlRoot +=`/merged/${dateParam}`;
}

async function scrape(url, sport) {
  var it = await scraper.scrape(url, sport);
  for (let g of it) {
    if (g.awayLine != undefined) {
      // game id
      //console.log(chalk`${g.id}\t{blue ${g.startTime}}\t{red.bold ${g.away.name}}\t{red ${g.awayLine}}\t\t{green.bold ${g.home.name}}\t{green ${g.homeLine}}\t\t{cyanBright ${g.totalLine}}\t\t{red ${g.score.away}}\t{red ${g.score.home}}`);
      // regular
      console.log(chalk`{blue ${g.startTime}}\t{red.bold ${g.away.name}}\t{red ${g.awayLine}}\t\t{green.bold ${g.home.name}}\t{green ${g.homeLine}}\t\t{cyanBright ${g.totalLine}}`);      
    }
  }
}

const argv = require('yargs')
  .version()
  .usage('Usage: sport-lines  <command> [options]')
  .alias('h', 'help')
  .option('date', {
    describe: 'date to scrape (yyyyMMdd)',
    default: 'today'
  })
  .option('sport', {
    describe: 'sport(s) to scrape (NFL, NCAAF, NFAAB, NBA, MLB)',
    default: 'TODAY (all sports)'
  })
  .option('status', {
    describe: '[WIP] what games to include; all (A), in-progress (IP), completed (C), not started(NS)',
    choices: ['A', 'C', 'IP', 'NS'],
    default: 'A'
  })
  .option('include-scores', {
    alias: 's',
    describe: 'should current scores be included'
  })
  .command('output', 'output scraped games to the console',
    (yargs) => {
    },
    (argv) => {
      console.log('doing some scraping - ' + JSON.stringify(argv));
      let url = todayURL;
      if (argv.date !== 'today') {
        scrape(getDateSpecificURL(argv.date, argv.sport), argv.sport);
      }
      else {
        scrape(url, argv.sport);
      }

    }
  )
  .command('save', 'save scraped games to a DB',
    (yargs) => {
      yargs.option('mongodb-url', {
        describe: 'the mongodb to save the data to',
        default: 'localhost'
      })
    },
    (argv) => {
      console.log('saving games to ' + argv['mongodb-url']);
    }
  )
  .command('*', 'default stuff',
    (argv) => {
      console.log('doing some default stuff')
    }
  )

  .epilogue('data pulled from www.sportsbookreview.com')
  .help()
  .argv;
